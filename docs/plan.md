# The TimeFlo Project: Project Plan
Bart Massey 2021

## Resources

Need a Debian box running Python3.

## Work Breakdown

1. Complete requirements doc. 15m

2. Complete project plan. 10m

3. Complete design doc. 15m

4. Implement TimeFlo. 1h

5. Complete V&V plan. 15m

6. Validate TimeFlo. 1h

## Schedule

Milestone 1: Complete by 11PM Wed 3 November
Milestone 2: Complete by 11PM Thu 4 November

## Milestones and Deliverables

1. Tasks 1-4 of Work Breakdown. Deliverables are implied.

2. Tasks 5-6 of Work Breakdown. Deliverables are implied.
