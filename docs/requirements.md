# The TimeFlo Project: Requirements Document
Bart Massey 2021

## Introduction

This project is an implementation of
[TimeFlo](https://d2l.pdx.edu/d2l/lms/dropbox/user/folder_submit_files.d2l?db=400685&grpid=0&isprv=0&bp=0&ou=885966),
a simplified version of Pomodoro™.

### Purpose and Scope

This document specifies MVP requirements for the TimeFlo
project.

### Target Audience

Developers, maintainers and customers of the TimeFlo
project.

### Terms and Definitions

N/A

## Product Overview

The user of TimeFlo will start a session whenever they begin
development work. They will be notified when a session break
is needed, and the break will be timed.

TimeFlo will be a command-line program. Notifications will
be limited to a countdown timer and a terminal "bell".

### Users and Stakeholders

Users include anyone that wants to use the TimeFlo approach
to development.

Bart Massey is the project developer. He is also the primary
customer. He is also the evaluator of the work.

### Use Case

We describe the intended use of the TimeFlo software here.

A user Bart wants a timed work session. They start the
TimeFlo timer on the command-line. TimeFlo starts a work
session timer that displays counting down in minutes and
seconds.  When the timer reaches zero, Bart is notified by a
terminal "bell" sound or flash. A break timer then starts
counting up. When Bart finishes the break, he interrupts
TimeFlo at the command line. This starts a new cycle.

When Bart completes use of TimeFlo, he interrupts TimeFlo
during the work session timer. This causes TimeFlo to exit.

## Functional Requirements

This section describes specific functional requirements for
TimeFlo.

### A. Invocation

1. The timer will be invoked from the command line.

2. The timer will have a session time of 20 minutes.

### B. Work Session Operation

1. The timer will display a count-down time in minutes and
   seconds since the start of the session, in the format
   'work  12m:34s'.

2. The time display will be updated every second.

3. When interrupted via the keyboard, TimeFlo will
   gracefully exit.

### C. End Of Work Session Operation

1. When a work session ends, TimeFlow will "ring the
   terminal bell" to notify the user to end the session.

2. TimeFlo will then enter Break Session Operation.

### D. Break Session Operation

1. TimeFlo will display a count-up time in minutes and
   seconds since the start of the break, in the format
   'break 12m:34s' or 'break 12h:34m'. The program will exit
   when a break time of 24 hours is reached.

2. The time display will be updated no less often than when
   the least significant digit changes: every second when
   displaying seconds, or every minute when displaying
   minutes.

3. When interrupted via the keyboard, TimeFlo will
   gracefully enter Work Session Operation.

## Extra-functional Requirements

These requirements are in addition to the functional
requirements.

### Efficiency

1. The running timer will not exceed 2% CPU consumption on a
   "typical" desktop CPU.

2. The running timer will not have a working-set memory
   footprint larger than 100KB on a "typical" desktop CPU.

### Accuracy

1. The timer will be accurate to within five seconds per 20
   minutes during a Work Session.

2. The timer will be accurate to within 30 seconds per 24
   hours during a Break Session.

### Language and Platform

1. TimeFlo will at minimum run on a modern Debian-derived
   Linux box with any Debian-supported architecture.
