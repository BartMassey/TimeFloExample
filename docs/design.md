# The TimeFlo Project: Design
Bart Massey 2021

## Introduction

This document provides a summary of the overall architecture
and detailed design of the TimeFlo Timer.

TimeFlo will be written in the Python programming language
version 3.

## Architecture

The timer consists of three basic components:

1. Work Loop: sequence the timers.

   The Work Loop will repeatedly call the Timer function
   with appropriate parameters for Work Session and then
   Break Session operation, signaling at the end
   of Work Session.

   Upon return from a session, the Work Loop will adjust
   the displayed line to "permanent" status. When returning
   from a Work Session, the line will be adjusted to show
   the Work Session time if return was normal; otherwise it
   will be adjusted to show the time completed before exiting.
   When returning from a Break Session, the line will be
   adjusted to show the break time. The adjustment will be
   completed by moving to the next line to start a new
   display: for example

             work  20m:00s
             break 05m:30s
             work  20m:00s
             break 02h:35m
             work  07m:12s

   The Work Loop will exit when the Timer function returns
   indication of interrrupt during Work Session.

   The Work Loop will run in the context of a keyboard
   interrupt exception handler that ignores keyboard
   interrupts.

2. Timer: Run a count-up or count-down timer with display;
   handles interrupts as specified.

   The timer will be a function that takes an initial value
   and a count direction (up or down) as arguments.

   The timer function will display the current timer value
   once per second while running. If the timer reaches zero,
   the timer function will stop and return 0 as an
   indication of expiry. The timer display will be
   in minutes/seconds mode when the time is less than
   one hour, and hours/minutes mode otherwise.

   The timer function will run in the context of a keyboard
   interrupt exception handler that causes the function to
   exit, returning the current timer value in seconds as
   indication of interrupt.
