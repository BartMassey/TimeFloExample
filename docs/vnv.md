# The TimeFlo Project: Validation and Verification
Bart Massey 2021

## Introduction

This document describes the V&amp;V plan for the TimeFlo
project, a work session timer.

### Purpose and Scope

This document describes the tests and inspections that will
be performed to verify and validate TimeFlo.

### Target Audience

TimeFlo developers.

### Terms and Definitions

N/A

## V&amp;V Plan Description

The V&amp;V of TimeFlo will proceed via unit test, limited
system test, and a developer code walkthrough. Some
instrumentation will be added to the code to facilitate
test.

## Test Plan Description

Appropriate units in the program will have unit tests. In
addition, the overall system will be tested using an
accelerated-time strategy.

### Testing Schedule

Unit tests, system tests, and inspection will be completed
by 13 November 2021.

### Release Criteria

All tests must pass. System must verifiably meet all critera.

## Unit Testing

The three units of the program are `format_time()`,
`timer()` and `clear()`. Unit-testing `clear()` is not
feasible. Unit-testing `timer()` will require implementing
an accelerated-time mode.

## System Testing

The system will be tested by running it in accelerated-time
mode and comparing the output to a desired output.

## Inspection

Code will be walked through carefully by the developer.
