# TimeFlo timer.
# Bart Massey 2021

import os, time

# Length of Work Session in seconds.
default_time = 20 * 60

# Accelerate for testing purposes: tick in seconds.
if 'TIMEFLO_TICK' in os.environ:
    tick_time = float(os.environ['TIMEFLO_TICK'])
else:
    tick_time = 1

def format_time(secs):
    """Return a time formatted as 12m:34s or
    as 12h:34m as appropriate.

    >>> format_time(0)
    '00m:00s'
    >>> format_time(1)
    '00m:01s'
    >>> format_time(10)
    '00m:10s'
    >>> format_time(61)
    '01m:01s'
    >>> format_time(60 * 60 + 1)
    '01h:00m'
    >>> format_time(60 * 60 + 59)
    '01h:00m'
    >>> format_time(60 * 60 + 60)
    '01h:01m'
    """
    one_hour = 60 * 60
    if secs >= one_hour:
        hours = secs // one_hour
        mins = (secs % one_hour) // 60
        return f"{hours:02d}h:{mins:02d}m"
    mins = secs // 60
    secs = secs % 60
    return f"{mins:02d}m:{secs:02d}s"
    

def timer(init_secs, increment, session, tick):
    """Set the timer value to init_secs.  Then loop incrementing
    the timer value and printing it once per second.  If a
    keyboard interrupt is received, return the current timer
    value.  Otherwise, return 0 if the timer reaches -1.
    The session string is used for formatting; all session
    strings must be the same length for proper operation.
    If accelerated, run at 100× speed for testing purposes.

    """
    timer_value = init_secs
    try:
        while timer_value >= 0:
            ts = format_time(timer_value)
            print(f"{session}{ts}", end="\r")
            # XXX Update once per tick regardless
            # of whether in hours/minutes mode.
            time.sleep(tick)
            timer_value += increment
        return max(timer_value, 0)
    except KeyboardInterrupt:
        return timer_value

def clear():
    """Clear the current line."""
    # XXX Conservative clearout: should be
    # length of session name plus 7 for 
    # time format.
    print("", end="\r")
    print(" " * 16, end="\r")

if 'TIMEFLO_TEST' in os.environ:
    import doctest
    doctest.testmod()
    exit(0)

# Work Loop
try:
    while True:
        # Run work session.
        remaining = timer(default_time, -1, "work  ", tick_time)
        # Set up for display.
        clear()
        # Test for completion.
        if remaining > 0:
            ts = format_time(remaining)
            print(f"\rwork  {ts}")
            break
        ts = format_time(default_time)
        print(f"\r\007\rwork  {ts}")
        # Run break session.
        break_time = timer(0, +1, "break ", tick_time)
        clear()
        ts = format_time(break_time)
        print(f"\rbreak {ts}")
        
except KeyboardInterrupt:
    pass
