# TimeFlo
Copyright &copy; 2021 Bart Massey

TimeFlo is an implementation of a
[Pomodoro&reg;](https://en.wikipedia.org/wiki/Pomodoro_Technique)-like
timer for breaking out of flow state.

TimeFlo is a command-line timer that "rings the
terminal bell" to terminate a work session. The timer runs
20 minute work sessions. Breaks are timed up to 24 hours, at
which point TimeFlo will exit.

To exit TimeFlo, interrupt it from the keyboard during a
work session. To finish a break, interrupt TimeFlo during a
break.

## Status and Roadmap

MVP TimeFlo functionality is currently complete and works at
least at a basic level.

* [x] Requirements complete.
* [x] Project plan complete.
* [x] Design complete.
* [x] Implementation complete.
* [x] V&amp;V plan complete.
* [ ] Validation complete.

## Install and Run

    python3 timeflo.py

### Linux Terminal Bell

By default, most Linux installations do not make a bell
character sound — this is considered an annoying anachronism
by many.  To enable bell sound for GnomeTerminal +
PulseAudio, follow
[these instructions](https://askubuntu.com/q/1310345).

## Development Docs

Development documentation is available for TimeFlo, including:

* [Requirements Specification](docs/reqs.md)
* [Project Plan](docs/plan.md)
* [Design Doc](docs/design.md)
* [V&amp;V Report](docs/vnv.md)

## Test

* To unit test using doctests:

        $ export TIMEFLO_TEST=1
        $ python3 timeflo.py
